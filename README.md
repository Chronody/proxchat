# ProxChat

This app is a Proof of Concept using Wifi beacons for localized communication in form of a 
Chat App by starting a hotspot with a message as the SSID to send a message to peers,
and recieving a list of available wifi networks and parsing that to recieve messages from peers.

Accompanying Blogpost going into more detail: https://chronody.gitlab.io/post/proxchat/