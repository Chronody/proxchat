package de.redist.proxchat;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import java.lang.reflect.Method;

public class APManager {

    private WifiManager wifiManager;
    private Context c;
    private WifiConfiguration wifiConfig;

    private Method enableWifiAP;

    public APManager(Context c, WifiManager wm, WifiConfiguration wifiConfig){
        this.wifiManager = wm;
        this.c = c;
        this.wifiConfig = wifiConfig;

        init();
    }

    public void init(){

        try {
            /*
            wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            wifiConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            wifiConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            wifiConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            wifiConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            wifiConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            wifiConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
*/
            enableWifiAP = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class, boolean.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }


    public void disable(){
        boolean result;

        try
        {
            wifiConfig.status = WifiConfiguration.Status.DISABLED;

            result = (Boolean) enableWifiAP.invoke(wifiManager, wifiConfig, false);

            wifiManager.setWifiEnabled(true);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            result = false;
            System.out.println("Enabling AP failed");
        }
    }

    public void enable(){
        boolean result;

        try
        {
            wifiManager.setWifiEnabled(false);

            WifiConfiguration  myConfig =  wifiConfig;

            myConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

            myConfig.status = WifiConfiguration.Status.ENABLED;
            result = (Boolean) enableWifiAP.invoke(wifiManager, myConfig, true);

            System.out.println(result);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            result = false;
        }
    }
}
