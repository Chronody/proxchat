package de.redist.proxchat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private Context c;

    private ArrayList<Message> data;

    public ChatAdapter(Context c,  ArrayList<Message> data){
        this.c = c;
        this.data = data;

    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row, parent, false);

        return new ViewHolder(rowView);
    }

    /*
    called for each item of the list
  */
    @Override
    public void onBindViewHolder(ChatAdapter.ViewHolder holder, int position) {
        Message m = data.get(position);

        holder.sender.setText(m.sender);
        holder.message.setText(m.message);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView sender, message;

        public ViewHolder(View itemView) {
            super(itemView);

            sender = (TextView) itemView.findViewById(R.id.sender);
            message = (TextView) itemView.findViewById(R.id.message);
        }
    }
}
