package de.redist.proxchat;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


/**
 * TODO: Autoscroll after Scan
 * TODO: Don't display static WifiAPs repeatedly in every Scan
 */
public class MainActivity extends AppCompatActivity {

    private RecyclerView chatLog;
    private LinearLayoutManager chatManager;
    private WifiHandler wifiHandler;
    private TextView chatInput;
    private TextView timerInput;
    private ChatAdapter chatAdapter;

    private ArrayList<Message> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data = new ArrayList();

        chatInput = findViewById(R.id.inputField);
        timerInput = findViewById(R.id.sendDurationInput);
        chatLog = findViewById(R.id.chatLog);

        chatManager = new LinearLayoutManager(this);
        chatLog.setLayoutManager(chatManager);

        chatAdapter = new ChatAdapter(this, data);
        chatLog.setAdapter(chatAdapter);

        wifiHandler = new WifiHandler(this, data, chatAdapter);

        registerReceiver(wifiHandler, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        Timer scanTimer = new Timer();

        scanTimer.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                wifiHandler.startScan();
                System.out.println("Called Scan");
            }
        },0, 10000);

        checkPermissions();
    }

    //WORKAROUND FOR ANDROID BUG
    //LOCATION SERVICES NEED TO BE ENABLED AND PERMITTED TO RECIEVE WIFI SSIDS
    //https://stackoverflow.com/questions/32151603/scan-results-available-action-return-empty-list-in-android-6-0/32151901#32151901
    private void checkPermissions(){

        if(checkSelfPermission("ACCESS_FINE_LOCATION") == PackageManager.PERMISSION_GRANTED){
            System.out.println("Can access fine location");
        }else{

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(getApplicationContext())) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, 200);
                }
            }

            final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 1001;

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);
                Toast.makeText(this, "Please enable and grant permission to Location services", Toast.LENGTH_SHORT);
            }else{
                System.out.println("Permissions already granted.");
            }
        }
    }

    public void send(View view){
        String input = chatInput.getText().toString();

        if(!(input.compareTo("") == 0)) {

            wifiHandler.setSSID(input);
            wifiHandler.startHS();

           new Timer().schedule(
                   new TimerTask() {
                       @Override
                       public void run() {
                            wifiHandler.stopHS();
                            System.out.println("Async finished.");
                       }
                   }, new Integer(timerInput.getText().toString()) * 1000);

            data.add(new Message("Me", input));

            chatInput.setText("");
            chatAdapter.notifyDataSetChanged();
            chatLog.scrollToPosition(data.size()-1);

        }
    }
}


