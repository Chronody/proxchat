package de.redist.proxchat;


public class Message {

    public String sender;
    public String message;
    public int timestamp;

    public Message(String sender, String message){
        this.sender = sender;
        this.message = message;
    }
}
