package de.redist.proxchat;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;

import java.util.ArrayList;
import java.util.List;

public class WifiHandler extends BroadcastReceiver{

    private WifiManager manager;
    private Context c;
    private ArrayList<Message> data;
    private ChatAdapter chatAdapter;

    private WifiConfiguration wifiConfig;

    private APManager apman;

    private boolean initial_scan = true;
    private ArrayList<String> blacklist;

    public WifiHandler(Context c, ArrayList<Message> d, ChatAdapter ca){
        this.c = c;
        this.data = d;
        this.chatAdapter = ca;

        manager = (WifiManager) c.getSystemService(c.WIFI_SERVICE);
        wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = "Hello Proxchat";

        apman = new APManager(c, manager, wifiConfig);
        blacklist = new ArrayList<String>(10);
    }

    public void startScan(){
        manager.startScan();
        System.out.println("Started Scan...");
    }

    public void startHS(){ apman.enable(); }

    public void stopHS(){ apman.disable(); }

    public void setSSID(String ssid){
        if(ssid.length() <= 32) wifiConfig.SSID = ssid;
    }

    public void getResults(){
        List<ScanResult> results = manager.getScanResults();

        if(initial_scan) {
            for (ScanResult r : results) {
                Message m = new Message(r.BSSID, r.SSID);
                blacklist.add(r.BSSID);
                data.add(m);
            }

            initial_scan = false;
        }

        for (ScanResult r : results) {
            Message m = new Message(r.BSSID, r.SSID);
            if(blacklist.indexOf(r.BSSID) == -1) {
                data.add(m);
            }
        }

        chatAdapter.notifyDataSetChanged();
        System.out.println("Scan Finished");
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        getResults();
    }

}
